require('dotenv').config()

const next = require('next')
const express = require('express')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const morgan = require('morgan')

const logger = require('./api/config/logger')

const routes = require('./routes')
const queueController = require('./api/controller/qclerkController')

const app = next({ dev: process.env.NODE_ENV !== 'production' })
const handler = routes.getRequestHandler(app)

app.prepare().then(() => {
  const PORT = process.env.PORT || 3000

  const server = express()
  server.use(express.static('static'))
  // server.use(morgan('{"timestamp":":date[iso]","logOrigin":"express","message":{"remoteAddress":":remote-addr","remoteUser":":remote-user","method":":method","url":":url","httpVersion":":http-version","status":":status","res":":res[content-length]","referrer":":referrer","userAgent":":user-agent","responseTime":":response-time"}}'))
  server.use(cookieParser())
  server.use(bodyParser.json())
  server.use('/api', queueController)
  server.use(handler).listen(PORT)
  logger.info({
    logOrigin: 'express',
    message: `QClerk start at port ${PORT}`,
  })
})
