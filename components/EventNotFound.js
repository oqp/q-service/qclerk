import React from 'react'
import propTypes from 'prop-types'
import styled from 'styled-components'

const Image = styled.img`
  display: block;

  @media (max-width: 575.98px) {
    position: fixed;
    z-index: -1;
    opacity: .5;
  }
`

const EventNotFound = ({ subdomain }) => (
  <div className='d-flex align-items-center justify-content-center flex-column vh-100'>
    <div className='px-5'>
      <div className='row'>
        <div className='col-12 col-sm-6 d-flex align-items-center'>
          <div>
            <h1 style={{ fontWeight: 'bold' }}>
              Oops..
            </h1>
            <h2>
              Something isn't right
            </h2>
            <div>
              {`There is no event running on the requested subdomain "${subdomain}"`}
            </div>
          </div>
        </div>
        <Image className='col-12 col-sm-6' src='/static/not-found.png' alt='event not found' />
      </div>
    </div>
    <div className='text-center' style={{ marginTop: '50px', color: '#BFBFBF' }}>Powered by OQP</div>
  </div>
)

EventNotFound.propTypes = {
  subdomain: propTypes.string.isRequired,
}
export default EventNotFound
