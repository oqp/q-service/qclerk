import React from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import styled from 'styled-components'

const ProgressBarContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  
  .progress {
    width: 100%;
    @media (min-width: 525px) {
      width: 70%;
    }
  }
`

const ProgressBar = ({ percent, paused }) => (
  <ProgressBarContainer>
    <div className='progress' style={{ height: '22px' }}>
      <div className={classnames('progress-bar', paused ? undefined : 'progress-bar-animated progress-bar-striped')} role='progressbar' style={{ width: `${percent}%` }} aria-valuenow={percent} aria-valuemin='0' aria-valuemax='100' />
    </div>
    <div style={{ paddingLeft: '14px' }}>
      {`${percent}%`}
    </div>
  </ProgressBarContainer>
)

ProgressBar.propTypes = {
  percent: PropTypes.number.isRequired,
  paused: PropTypes.boolean,
}

export default ProgressBar
