import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const ButtonStyled = styled.button`
  background-color: #0C7698;
  border: 1px solid #005066;
  border-radius: 3px;
  padding: 7px 5px;
  ${props => props.block && 'width: 100%'}
  color: #FFF;
`

export const MainButton = styled.button`
  background-color: #FFF;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.25);
  border-radius: 4px;
  border: 1px solid ${props => props.theme.colors.yellow.default};
  width: ${props => (props.block ? '100%' : 'auto')};
  padding: ${props => (props.block ? 'initial' : '0 42px')};
  height: 30px;
  color: ${props => props.theme.colors.yellow.default};
  text-align: center;
  font-size: 12px;
  transition: all .1s linear;
  &:hover {
    background-color: ${props => (props.theme.colors.yellow.default)};
    color: #FFF;
    border-color: ${props => (props.theme.colors.yellow.default)};
  }
  &:focus {
    background-color: ${props => (props.theme.colors.yellow.focus)};
    color: #FFF;
    border-color: ${props => (props.theme.colors.yellow.focus)};
  }
  &:disabled {
    background-color: #FFF;
    color: ${props => (props.theme.colors.yellow.disabled)};
    border-color: ${props => (props.theme.colors.yellow.disabled)};
    cursor: not-allowed;
  }
  .input-group-append & {
    border-top-left-radius: 0;
    border-bottom-left-radius: 0;
    height: auto;

    &:disabled {
      box-shadow: initial;
    }
  }
`

export const SecondaryButton = styled(MainButton)`
  border-color: #3A559F;
  color: #3A559F;
  &:hover {
    background-color: #3A559F;
    color: #FFF;
    border-color: #3A559F;
  }
  &:focus {
    background-color: #3A559F;
    color: #FFF;
    border-color: #3A559F;
  }
  &:disabled {
    background-color: #FFF;
    color: #E7E7E7;
    border-color: #E7E7E7;
  }
`

const Button = ({ children, ...props }) => (
  <ButtonStyled {...props}>{children}</ButtonStyled>
)

Button.propTypes = {
  children: PropTypes.func.isRequired,
}

export default Button
