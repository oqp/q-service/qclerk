import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const PageStyled = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
  justify-content: center;
  align-self: center;
`

const Page = ({ children }) => (
  <PageStyled>
    {children}
  </PageStyled>
)

Page.propTypes = {
  children: PropTypes.element.isRequired,
}

export default Page
