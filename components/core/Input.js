import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const InputStyled = styled.input`
  background-color: #FFF;
  box-shadow: inset 0px 1px 4px rgba(0, 0, 0, 0.15);
  border-radius: 3px;
  padding: 6px 5px;
  width: 100%;
`

const Input = ({ children, ...props }) => (
  <InputStyled {...props}>{children}</InputStyled>
)

Input.propTypes = {
  children: PropTypes.func.isRequired,
}

export default Input
