import React from 'react'
import propTypes from 'prop-types'
import styled from 'styled-components'
import moment from 'moment'

const QueueClosed = ({
  event,
  endTime,
}) => (
  <div className='d-flex align-items-center justify-content-center flex-column vh-100 px-3'>
    {event.imageURL && (
      <div className='mb-4'>
        <div className='col-12 text-center'>
          <div>
            <img className='img-fluid' alt='logo' style={{ height: '80px' }} src={event.imageURL} />
          </div>
        </div>
      </div>
    )}
    <h1 className='text-center' style={{ fontSize: '30px', fontWeight: 'bold' }}>
      {`${event.name}`}
    </h1>
    <h2 className='text-center'>
      The queue is CLOSED
    </h2>
    <div className='text-center'>
      {'We are glad you are interested in our event. Please keep in touch for our upcoming events.'}
    </div>
    <div className='text-center' style={{ marginTop: '10px' }}>
      Hope to have you next time
    </div>
    <div className='text-center' style={{ marginTop: '50px', color: '#BFBFBF' }}>Powered by OQP</div>
  </div>
)

QueueClosed.propTypes = {
  event: propTypes.instanceOf(propTypes.object).isRequired,
  endTime: propTypes.objectOf(Date).isRequired,
}
export default QueueClosed
