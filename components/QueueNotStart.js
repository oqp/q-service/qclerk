import React from 'react'
import propTypes from 'prop-types'
import moment from 'moment'

const QueueNotStart = ({
  event,
  startTime,
}) => (
  <div className='d-flex align-items-center justify-content-center flex-column vh-100 px-3'>
    {event.imageURL && (
      <div className='mb-4'>
        <div className='col-12 text-center'>
          <div>
            <img className='img-fluid' alt='logo' style={{ height: '80px' }} src={event.imageURL} />
          </div>
        </div>
      </div>
    )}
    <h1 className='text-center' style={{ fontSize: '30px', fontWeight: 'bold' }}>
      {`${event.name}`}
    </h1>
    <h2 className='text-center'>
      The queue is not yet open
    </h2>
    <div className='text-center'>
      {`Please come visit again at "${moment(startTime).toLocaleString()}"`}
    </div>
    <div className='text-center' style={{ marginTop: '50px', color: '#BFBFBF' }}>Powered by OQP</div>
  </div>
)

QueueNotStart.propTypes = {
  event: propTypes.instanceOf(propTypes.object).isRequired,
  startTime: propTypes.objectOf(Date).isRequired,
}
export default QueueNotStart
