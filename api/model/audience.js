const mongoose = require('mongoose')

const { Schema, SchemaTypes } = mongoose

const audienceSchema = new Schema({
  _id: SchemaTypes.ObjectId,
  uuid: { type: String, index: true },
  queueNumber: Number,
  telno: String,
  email: String,
  enteredQueueAt: Date,
  status: { type: String, index: true },
  eligibledAt: { type: Date, index: true },
  servedAt: { type: Date, index: true },
  canceledAt: Date,
})

const AudienceModel = mongoose.model && mongoose.models.Audience ? mongoose.models.Audience : mongoose.model('Audience', audienceSchema)

module.exports = { audienceSchema, AudienceModel }
