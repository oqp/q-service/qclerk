const mongoose = require('mongoose')

const { Schema, SchemaTypes } = mongoose

const waitingRoomSchema = new Schema({
  _id: SchemaTypes.ObjectId,
  website: String,
  uuid: String,
  name: String,
})

// need to check for existing model or it'll cause model overwrite
const WaitingRoomModel = mongoose.model && mongoose.models.WaitingRoom ? mongoose.models.WaitingRoom : mongoose.model('WaitingRoom', waitingRoomSchema)

module.exports = { waitingRoomSchema, WaitingRoomModel }
