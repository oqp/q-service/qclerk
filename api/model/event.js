const mongoose = require('mongoose')

const { Schema, SchemaTypes } = mongoose

const eventSchema = new Schema({
  _id: SchemaTypes.ObjectId,
  uuid: { type: String, index: true },
  name: String,
  openTime: Date,
  closeTime: Date,
  maxOutflowAmount: Number,
  sessionTime: Number,
  currentQueueNumber: Number,
  redirectURL: String,
  isActive: Boolean,
  waitingRoomId: String,
  integrateToken: String,
  subdomain: { type: String, index: true },
  imageURL: String,
})

const EventModel = mongoose.model && mongoose.models.Event ? mongoose.models.Event : mongoose.model('Event', eventSchema)

module.exports = { eventSchema, EventModel }
