require('dotenv').config('/')
const express = require('express')

const router = express.Router()

// must implicitly import AudienceModel in order to use Audience model
require('../model/audience')
const { EventModel } = require('../model/event')
const AudienceStatus = require('../config/AudienceStatus')
const { mongoose } = require('../../api/config/mongoose')
const kafkaProducer = require('../config/kafkaClient').producer
const logger = require('../config/logger')
const subdomainExtractor = require('../../utils/subdomainExtractor')

router.get('/startup', async (req, res) => {
  res.status(200).json({
    timestamp: Date.now(),
    message: 'Ready to accept connection',
  })
})

router.post('/updateEligibledToServing', async (req, res) => {
  const { audienceUUID } = req.body
  const subdomain = subdomainExtractor.getSubdomainFromDomain(req.headers.host)
  const event = await EventModel.findOne({ subdomain }, { _id: 0 })
  const connection = mongoose.connection.useDb(`event-${event.uuid}`)
  const TempAudienceModel = connection.model('Audience')
  let audience = await TempAudienceModel.findOne({ uuid: audienceUUID }, { _id: 0, waitingRoomId: 0 })
  if (audience === null) {
    res.status(404).json({ message: 'Audience Not Found' })
  } else if (audience.status === AudienceStatus.ELIGIBLED) {
    audience.status = AudienceStatus.SERVING
    audience.servedAt = new Date()
    audience = await TempAudienceModel.findOneAndUpdate({ uuid: audienceUUID }, audience)
    res.status(200).json(audience)
  } else {
    res.status(400).json({ message: 'Invalid Audience Status' })
  }
})

router.get('/qinfo', async (req, res) => {
  const { audienceUUID } = req.query
  const subdomain = subdomainExtractor.getSubdomainFromDomain(req.headers.host)
  const event = await EventModel.findOne({ subdomain }, { _id: 0 })
  try {
    delete event.integrateToken
  } catch (er) {
    logger.debug(er.toString())
  }
  const connection = mongoose.connection.useDb(`event-${event.uuid}`)
  const TempAudienceModel = connection.model('Audience')
  const audience = await TempAudienceModel.findOne({ uuid: audienceUUID }, { _id: 0, waitingRoomId: 0 })
  const response = {
    audienceStatus: audience,
    event,
  }
  return res.status(200).json(response)
})

router.post('/assign', async (req, res) => {
  const LOG_ORIGIN = 'kafkaProducer'
  const { audienceUUID } = req.body
  const subdomain = subdomainExtractor.getSubdomainFromDomain(req.headers.host)
  const event = await EventModel.findOne({ subdomain }, { _id: 0 })
  const messageValue = { audienceUUID, eventUUID: event.uuid }
  return kafkaProducer.send([{
    topic: process.env.KAFKA_ASSIGN_QUEUE_TOPIC_NAME,
    messages: [JSON.stringify(messageValue)],
    key: 'queueInitialData',
    partition: 0,
    attributes: 0,
    timestamp: Date.now(),
  }], (error, data) => {
    if (error) {
      logger.error(null, {
        logOrigin: LOG_ORIGIN,
        error,
      })
      res.status(500).json({
        message: 'Failed to publish message',
      })
    }
    logger.info({
      logOrigin: LOG_ORIGIN,
      message: data,
    })
    res.status(200).json({
      message: 'Message published',
    })
  })
})

module.exports = router
