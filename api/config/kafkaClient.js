require('dotenv').config()

const kafka = require('kafka-node')

const client = new kafka.KafkaClient({
  kafkaHost: process.env.KAFKA_HOST,
  idleConnection: process.env.KAFKA_MAX_IDLE_TIME_MS || 300000,
  reconnectOnIdle: true,
})

const producer = new kafka.Producer(client)

module.exports = { client, producer }
