const AudienceStatus = {
  COMPLETED: 'completed',
  WAITING: 'waiting',
  SERVING: 'serving',
  ELIGIBLED: 'eligibled',
  ARRIVAL_TIMEDOUT: 'arrival-timedout',
  SESSION_TIMEDOUT: 'session-timedout',
  CANCELED: 'canceled',
}

module.exports = AudienceStatus
