require('dotenv').config('../../')

const mongoose = require('mongoose')

const logger = require('./logger')

mongoose.set('bufferCommands', false)
mongoose.connect(`mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`, { useNewUrlParser: true, connectTimeoutMS: 2000 })
const LOG_ORIGIN = 'mongoose'
const db = mongoose.connection
db.on('error', (error) => {
  logger.error(null, {
    logOrigin: LOG_ORIGIN,
    error,
  })
})
db.once('open', () => {
  logger.info({
    logOrigin: LOG_ORIGIN,
    message: 'connected to mongodb server',
  })
})

module.exports = { db, mongoose }
