/* eslint no-console: off */

require('dotenv').config()

const http = require('http')

const request = http.request(`http://localhost:${process.env.PORT}/api/startup`, (res) => {
  console.log(`STATUS: ${res.statusCode}`)
  if (res.statusCode === 200) {
    process.exit(0)
  } else {
    process.exit(1)
  }
})

request.on('error', (err) => {
  console.log('ERROR', err)
  process.exit(1)
})

request.end()
