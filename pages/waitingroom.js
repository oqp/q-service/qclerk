/* eslint-disable global-require */
import React from 'react'
import PropTypes from 'prop-types'
import axios from 'axios'
import cookie from 'js-cookie'
import moment from 'moment'
import styled from 'styled-components'

import { Row, Col, Container } from 'reactstrap'
import getConfig from 'next/config'
import PageComponent from '../components/core/Page'
import { SecondaryButton } from '../components/core/Button'
import ProgressBar from '../components/ProgressBar'
import Input from '../components/core/Input'
import EventNotFound from '../components/EventNotFound'
import QueueNotStart from '../components/QueueNotStart'
import QueueClosed from '../components/QueueClosed'
import AudienceStatus from '../config/AudienceStatus'

const HappyPeopleImage = styled.img`
  width: 300px;
`

const isUUIDv5FormatValid = (uuid) => {
  const uuidV5RegEx = /^[0-9A-F]{8}-[0-9A-F]{4}-[5][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i
  return uuidV5RegEx.test(uuid)
}

const LOG_ORIGIN = 'waitingRoomPage'

const config = getConfig().publicRuntimeConfig

class WaitingRoomPage extends React.Component {
  static async getInitialProps(ctx) {
    const { req } = ctx
    const nookies = require('nookies')
    const cookies = nookies.parseCookies(ctx)
    let { audienceUUID } = cookies
    let event = null
    let redirectURL = ''
    let subdomain = ''
    let momentQueueEndTime = ''
    let momentQueueStartTime = ''
    let isBeforeStartTime = true
    let isAfterEndTime = true
    let isBetweenTime = false

    if (!process.browser) {
      const logger = require('../api/config/logger')
      const SubdomainExtractor = require('../utils/subdomainExtractor')
      subdomain = SubdomainExtractor.getSubdomainFromDomain(req.headers.host)
      require('dotenv').config()
      try {
        const { EventModel } = require('../api/model/event')
        event = await EventModel.findOne({ subdomain }, { _id: 0 })
        redirectURL = event.redirectURL.toString()
        momentQueueStartTime = moment(event.openTime).utc()
        momentQueueEndTime = moment(event.closeTime).utc()
        const currentTime = moment().utc()

        isBeforeStartTime = currentTime.isBefore(momentQueueStartTime)
        isAfterEndTime = currentTime.isAfter(momentQueueEndTime)

        isBetweenTime = isBeforeStartTime === false && isAfterEndTime === false

        if (isUUIDv5FormatValid(audienceUUID) === false && event !== null && isBetweenTime) {
          const uuidv5 = require('uuid/v5')
          const uuidv4 = require('uuid/v4')
          audienceUUID = uuidv5('audience', uuidv4())
          nookies.setCookie(ctx, 'audienceUUID', audienceUUID, { maxAge: 30 * 24 * 60 * 60 })
          const messageValue = {
            audienceUUID,
          }
          try {
            await axios.post(`http://localhost:${process.env.PORT || 3000}/api/assign`, messageValue, {
              headers: { Host: req.headers.host },
            })
            logger.info('Audience data is dispatched to QAssigner', {
              logOrigin: LOG_ORIGIN,
              topic: 'Audience dispatch',
              audienceUUID,
              eventUUID: event.uuid,
              status: 'success',
            })
          } catch (err) {
            logger.error(null, {
              logOrigin: LOG_ORIGIN,
              error: err,
              topic: 'Audience dispatch',
              audienceUUID,
              eventUUID: event.uuid,
              status: 'failed',
            })
          }
        }
      } catch (err) {
        logger.error(null, {
          logOrigin: LOG_ORIGIN,
          error: err.toString(),
        })
      }
    }

    return {
      audienceUUID, event, redirectURL, subdomain, isBeforeStartTime, isAfterEndTime, isBetweenTime,
    }
  }

  constructor(props) {
    super(props)
    this.state = {
      audienceStatus: '',
      isFirstPollingAttempt: true,
      updatedAt: null,
    }
  }

  componentDidMount() {
    const { audienceUUID, isBetweenTime, isAfterEndTime } = this.props
    if (audienceUUID && (isBetweenTime || isAfterEndTime)) {
      this.startPolling()
    }
  }

  clearInterval() {
    clearTimeout(this.timer)
  }

  async startPolling() {
    const { audienceUUID } = this.props
    try {
      const result = axios.get(`/api/qinfo?audienceUUID=${audienceUUID}`)
      const data = await result
      this.setState({
        audienceStatus: data.data.audienceStatus,
        event: data.data.event,
        isFirstPollingAttempt: false,
        updatedAt: moment().toLocaleString(),
      })
    } catch (error) {
      console.error(error)
    }
    this.timer = setTimeout(async () => this.startPolling(), 3000)
  }

  clearCookies() {
    cookie.remove('audienceUUID')
  }

  calculatePercent() {
    let percent = 0
    const { event: polledEvent, audienceStatus } = this.state
    if (polledEvent && audienceStatus && polledEvent.currentQueueNumber && audienceStatus.queueNumber) {
      percent = Math.floor((polledEvent.currentQueueNumber / audienceStatus.queueNumber) * 100)
      percent = percent <= 100 ? percent : 100
    }
    return percent
  }

  requestNewQueue() {
    this.clearCookies()
    window.location.reload(false)
  }

  async updateEligibledToServing(audienceUUID) {
    const result = await axios.post('/api/updateEligibledToServing', {
      audienceUUID,
    })
    return result
  }

  async checkAudienceProcess(audienceStatus) {
    const { audienceUUID, redirectURL } = this.props
    const redirectURLWithAudience = `${redirectURL}?audienceUUID=${audienceUUID}`
    if (audienceStatus === AudienceStatus.ELIGIBLED) {
      await this.updateEligibledToServing(audienceUUID)
      window.location.href = redirectURLWithAudience
    } else if (audienceStatus === AudienceStatus.SERVING) {
      window.location.href = redirectURLWithAudience
    }
  }

  checkIsWaiting(audienceStatus) {
    if (audienceStatus) {
      const { status } = audienceStatus
      if (status === AudienceStatus.WAITING || status === AudienceStatus.ELIGIBLED || status === AudienceStatus.SERVING) {
        return true
      }
    }
    return false
  }

  render() {
    const {
      audienceUUID, event, subdomain, isBeforeStartTime, isAfterEndTime,
    } = this.props
    if (!event) {
      return <EventNotFound subdomain={subdomain} />
    }

    if (isBeforeStartTime) {
      return <QueueNotStart event={event} startTime={event.openTime} />
    }
    if (isAfterEndTime && (audienceUUID === null || audienceUUID === undefined)) {
      return <QueueClosed event={event} endTime={event.closeTime} />
    }

    const {
      audienceStatus, event: polledEvent, isFirstPollingAttempt, updatedAt,
    } = this.state
    if (audienceStatus) {
      this.checkAudienceProcess(audienceStatus.status)
    }
    return (
      <PageComponent>
        <Container className='align-self-center align-items-center'>
          {event.imageURL && (
            <Row>
              <Col sm={12} className='text-center'>
                <div>
                  <img className='img-fluid' alt='logo' style={{ height: '80px' }} src={event.imageURL} />
                </div>
              </Col>
            </Row>
          )}
          {isFirstPollingAttempt ? (
            <div className='text-center'>
              Loading.. Just a moment
              {' :)'}
            </div>
          ) : (
            <>
              {this.checkIsWaiting(audienceStatus) && (
                <div>
                  <Row style={{ marginTop: '10px' }}>
                    <Col sm={12} className='text-center' style={{ fontSize: '30px', fontWeight: 'bold' }}>
                      You are now in line
                    </Col>
                  </Row>
                  <Row style={{ marginTop: '10px' }}>
                    <Col sm={12} className='text-center' style={{ fontSize: '16px' }}>
                      <div>
                        You are line up for event
                        {` '${event.name}'`}
                      </div>
                      <div style={{ marginTop: '10px' }}>
                        <div>
                          {polledEvent.arrivalTime ? (
                            <span>
                              {`When it is your turn, you will have ${polledEvent.arrivalTime / 60} minutes to arrive`}
                              <br />
                              {`${polledEvent.arrivalTime && 'and '}you have to complete your transaction on the destination website within ${polledEvent.sessionTime / 60} minutes`}
                            </span>
                          ) : (
                            <span>
                              {`When it is your turn, you will be redirected to the destination website, and you have to complete your transaction within ${polledEvent.sessionTime / 60} minutes`}
                            </span>
                          )}
                        </div>
                      </div>
                      <div style={{ marginTop: '10px' }}>
                        {`After ${polledEvent.sessionTime / 60} minutes, you will not be able to do any action on the destination website`}
                      </div>
                    </Col>
                  </Row>
                  <Row style={{ marginTop: '41px' }}>
                    <Col xs={12}>
                      <div className='text-center'>
                        <div style={{ fontSize: '25px' }}>
                          Waiting for
                        </div>
                        <div style={{ fontSize: '60px', fontWeight: 'bold' }}>
                          {audienceStatus && `${audienceStatus.queueNumber - polledEvent.currentQueueNumber}`}
                          <span style={{ fontSize: '30%', fontWeight: 'normal' }}>
                            {/* queue(s) */}
                            {/* queue(s) ahead of you */}
                          </span>
                        </div>
                        <div style={{ fontSize: '20px' }}>
                          queue(s)
                          {/* Queue No.
                          {' '}
                          {audienceStatus && audienceStatus.queueNumber} */}
                        </div>
                      </div>
                    </Col>
                  </Row>
                  <Row style={{ marginTop: '41px' }}>
                    <Col xs={12}>
                      <ProgressBar paused={!polledEvent.isActive} percent={this.calculatePercent()} />
                    </Col>
                  </Row>
                  {/* <Row style={{ marginTop: '37px' }}>
                    <Col>
                      <div className='text-center'>Please notify when it is my turn</div>
                      <div style={{ marginTop: '13px' }}><Input placeholder='Enter email address' /></div>
                      <button type='button' className='btn btn-primary btn-block' style={{ marginTop: '10px' }}>Notify me by email</button>
                    </Col>
                  </Row> */}
                  {polledEvent.isActive === false && (
                    <Row style={{ marginBottom: '14px' }}>
                      <Col>
                        <div className='text-center' style={{ fontSize: '25px', fontWeight: 'bold' }}>
                          The queue is paused
                        </div>
                      </Col>
                    </Row>
                  )}
                  <Row>
                    <Col>
                      <div className='text-center' style={{ fontSize: '16px', color: '#959595' }}>
                        Last status updated:
                        {' '}
                        {updatedAt}
                      </div>
                    </Col>
                  </Row>
                </div>
              )}
              {audienceStatus && audienceStatus.status === AudienceStatus.SESSION_TIMEDOUT && (
                <div>
                  <Row style={{ marginTop: '10px' }}>
                    <Col>
                      <div className='text-center' style={{ fontSize: '30px', fontWeight: 'bold' }}>
                        Your time in the system is up
                      </div>
                    </Col>
                  </Row>
                  <Row style={{ marginTop: '10px' }}>
                    <Col>
                      <div className='text-center'>
                        <span>
                          You did not complete the transaction within
                        </span>
                        <span>
                          {` ${polledEvent.sessionTime / 60} `}
                        </span>
                        <span>
                          minutes.
                        </span>
                      </div>
                      <div className='text-center'>
                        <span>
                          If you would like to get in the system again, please click the below button to get in line
                        </span>
                      </div>
                    </Col>
                  </Row>
                  <Row style={{ marginTop: '30px' }}>
                    <Col className='text-center'>
                      <SecondaryButton block className='text-center w-50' onClick={() => this.requestNewQueue()}>Get new queue number</SecondaryButton>
                    </Col>
                  </Row>
                </div>
              )}
              {audienceStatus && audienceStatus.status === AudienceStatus.COMPLETED && (
                <div>
                  <Row style={{ marginTop: '10px' }}>
                    <Col>
                      <div className='text-center' style={{ fontSize: '30px', fontWeight: 'bold' }}>Transaction is complete</div>
                    </Col>
                  </Row>
                  <Row style={{ marginTop: '10px' }}>
                    <Col>
                      <div className='text-center'>
                        Thank you for your interest. If you want to get in the system again,
                      </div>
                      <div className='text-center'>
                        please press the below button to get in line
                      </div>
                    </Col>
                  </Row>
                  <Row style={{ marginTop: '10px' }}>
                    <Col className='text-center'>
                      <HappyPeopleImage src='/static/happy-people.png' alt='happy people' className='img-fluid' />
                    </Col>
                  </Row>
                  <Row style={{ marginTop: '30px' }}>
                    <Col className='text-center'>
                      <SecondaryButton block className='text-center w-50' onClick={() => this.requestNewQueue()}>Get new queue number</SecondaryButton>
                    </Col>
                  </Row>
                </div>
              )}
              {audienceStatus && audienceStatus.status === AudienceStatus.ARRIVAL_TIMEDOUT && (
                <div>
                  <Row style={{ marginTop: '10px' }}>
                    <Col>
                      <div className='text-center' style={{ fontSize: '30px', fontWeight: 'bold' }}>Your queue is passed</div>
                    </Col>
                  </Row>
                  <Row style={{ marginTop: '10px' }}>
                    <Col>
                      <div className='text-center'>
                        <span>
                          We apologize, your queue is passed because you did not arrive within
                        </span>
                        <span>
                          {` ${polledEvent.arrivalTime / 60} `}
                        </span>
                        <span>
                          minutes of the calling time.
                        </span>
                      </div>
                      <div className='text-center'>
                        If you want to get in the system again, please press the below button to get in line
                      </div>
                    </Col>
                  </Row>
                  <Row style={{ marginTop: '30px' }}>
                    <Col className='text-center'>
                      <SecondaryButton block className='text-center w-50' onClick={() => this.requestNewQueue()}>Get new queue number</SecondaryButton>
                    </Col>
                  </Row>
                </div>
              )}
            </>
          )}
          <div className='text-center' style={{ marginTop: '50px', color: '#BFBFBF' }}>Powered by OQP</div>
          {
            config.NODE_ENV.toLowerCase() !== 'production' ? (
              <>
                <div>
                  <button type='button' onClick={() => this.clearInterval()}>Stop Polling</button>
                  <button type='button' onClick={() => this.startPolling()}>Start Polling</button>
                  <button type='button' onClick={() => this.clearCookies()}>Clear Cookies</button>
                </div>
                {audienceUUID}
                <div>
                  Status: &nbsp;
                  {audienceStatus && audienceStatus.status}
                </div>
                <div>
                  Current Queue: &nbsp;
                  {polledEvent && polledEvent.currentQueueNumber}
                </div>
              </>
            ) : null
          }
        </Container>
      </PageComponent>
    )
  }
}

WaitingRoomPage.defaultProps = {
  audienceUUID: null,
}

WaitingRoomPage.propTypes = {
  audienceUUID: PropTypes.string,
  event: PropTypes.instanceOf(Object).isRequired,
  subdomain: PropTypes.string.isRequired,
  redirectURL: PropTypes.string.isRequired,
  isBeforeStartTime: PropTypes.bool.isRequired,
  isAfterEndTime: PropTypes.bool.isRequired,
  isBetweenTime: PropTypes.bool.isRequired,
}

export default WaitingRoomPage
