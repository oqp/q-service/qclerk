
import NextJsApp, { Container } from 'next/app'

import { ThemeProvider, createGlobalStyle } from 'styled-components'

import 'bootstrap/dist/css/bootstrap.css'
import defaultTheme from '../config/theme/default'

const GlobalStyle = createGlobalStyle`
  body {
    @font-face {
      font-family: ${props => props.theme.primaryFont};
      src: url('${props => props.theme.primaryFontSource}')
    }
    font-family: ${props => props.theme.primaryFont};
    color: ${props => props.theme.colors.darkgrey.default}
    a {
      color: ${props => props.theme.colors.indigo.default};
    }
    a:link {
      color: ${props => props.theme.colors.indigo.default};
    }
    a:hover {
      color: ${props => props.theme.colors.indigo.default};
      text-decoration: underline;
    }
  }
`

class App extends NextJsApp {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {}

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx)
    }

    return { pageProps }
  }

  render() {
    const { Component, pageProps } = this.props

    return (
      <Container>
        <ThemeProvider theme={defaultTheme}>
          <>
            <GlobalStyle />
            <Component {...pageProps} />
          </>
        </ThemeProvider>
      </Container>
    )
  }
}

export default App
