require('dotenv').config()

const withCSS = require('@zeit/next-css')

module.exports = withCSS({
  serverRuntimeConfig: {
    SECRET: process.env.SECRET,
    KAFKA_HOST: process.env.KAFKA_HOST,
    KAFKA_PORT: process.env.KAFKA_PORT,
    DB_HOST: process.env.DB_HOST,
    DB_PORT: process.env.DB_PORT,
  },
  publicRuntimeConfig: {
    NODE_ENV: process.env.NODE_ENV || 'development',
  },
  webpack: (config, { isServer }) => {
    if (!isServer) {
      // eslint-disable-next-line no-param-reassign
      config.node = {
        fs: 'empty',
      }
    }
    return config
  },
})
