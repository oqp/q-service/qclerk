const getSubdomainFromDomain = (domain) => {
  const subdomain = domain.match(/.*?(?=\.)/)[0]
  return subdomain
}

module.exports = { getSubdomainFromDomain }
