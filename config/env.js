import getConfig from 'next/config'

const ENV = getConfig().serverRuntimeConfig
Object.assign(ENV, getConfig().publicRuntimeConfig)

export default ENV
