export default {
  name: 'Default',
  primaryFont: 'Helvetica',
  primaryFontSource: '/static/font/Helvetica.ttf',
  primaryTextColor: '#33353F',
  colors: {
    yellow: {
      default: '#FFC71B',
      focus: '#F4B903',
      disabled: '#FFE9A7',
    },
    green: {
      default: '#28A745',
      focus: '#28893E',
      disabled: '#BBF0C7',
    },
    red: {
      default: '#DC3545',
      focus: '#BA2E3B',
      disabled: '#EAB6BB',
    },
    salmon: {
      default: '#F0785A',
    },
    darkgrey: {
      default: '#555',
    },
    lightblue: {
      default: '#9FD6EE',
    },
    indigo: {
      default: '#3A559F',
    },
  },
}
